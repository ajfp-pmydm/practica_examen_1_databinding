package net.iescierva.ajfp.sesion1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import net.iescierva.ajfp.sesion1.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    /** Para que funcione el binding hay que añadir la parte comentada del gradle.settings(app)
     * y también modificar el XML de la clase a hacer binding (mirar activity_main.xml) **/

    boolean second = false;
    long num1 , num2 = 0;
    public static ActivityMainBinding dataBind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataBind = DataBindingUtil.setContentView(this, R.layout.activity_main);
    }

    /**Los métodos públicos los asociamos a eventos como el click, los privados los usarán los públicos **/
    public void sumarMostrarDatos(View view){
        String contenido = dataBind.cajaNumero.getText().toString();
        /**
        El patrón se puede definir así para poder usarse con matches distintos
        Pattern pat = Pattern.compile("\\d*");
        Matcher match = pat.matcher(contenido);
        boolean matches = match.matches();
        **/
        if (contenido.isEmpty()){
            Toast.makeText(this, "Introduzca datos, por favor.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!second){
            num1 = Long.parseLong(contenido);
            second = true;
            dataBind.boton1.setText("SUMAR");
        } else {
            num2 = Long.parseLong(contenido);
            dataBind.boton1.setText("GUARDAR");
            second = false;
            /**Usamos los Intent para manejar las actividades, en este caso creamos uno para comunicar dos actividades **/
            Intent intent = new Intent(this, ResultActivity.class)
                    .putExtra("num1", num1)
                    .putExtra("num2", num2);
            this.startActivity(intent);
        }
        dataBind.cajaNumero.setText("");
    }
}